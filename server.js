import dotenv from 'dotenv'
dotenv.config()

import express from 'express'
import mongoose from 'mongoose'
import { Recipe,difficultyEnum,priceEnum} from './model/recipe.js'
import { createControllers } from './controllers/recipes.js'
const recipesControllers = createControllers(Recipe,difficultyEnum,priceEnum)
import { createRouter } from './routes/recipes.js'
const recipesRouter = createRouter(recipesControllers)
// import { initialize as initUnleashClient } from 'unleash-client'

// import errorHandler from 'errorhandler'
// import cors from 'cors'

// const recipes = [{id:1,name:"brocolis","difficulty":"facile"},{id:2,name:"gratin","difficulty":"facile"}]

const app = express()
// app.use(cors())

app.set('views','./views')
app.set('view engine','pug')

app.use(express.urlencoded({extended:false}))

app.use("/recipes",recipesRouter)

app.get('/', (req, res, ) => {
    res.redirect('/recipes')
})

if (process.env.NODE_ENV === 'development') {
    console.log("debug");
    // app.use(errorHandler())

    // app.get('/*', (req, res) => {
    //     res.send('erreur')
    // })
}

const PASSWORD = "@h7%hTx__3(h7%hTx__3(bFsNB4@bFsNB4%7L"
console.log(PASSWORD)
const DB_PASSWORD = "@bFsNB4%7LQ7abZ!m9$2Q7abZ!m9$2@h7%hTx__"
console.log(DB_PASSWORD)
const JWT_SECRET = "Q7abZ!m9$2@bFsNB4%7LQ7abZ!m9$2@h7%hTx__"
console.log(JWT_SECRET)


// const unleash = initUnleashClient({
//     url: process.env.FF_URL,
//     appName: 'recipes',
//     customHeaders: { Authorization: process.env.FF_INSTANCE_ID },
// });

mongoose.connect(process.env.DB).then(()=>{
    console.log("connected to DB...");
    app.listen(process.env.PORT, () => {
        console.log(`server has started on port ${process.env.PORT}...`);
    })
})

