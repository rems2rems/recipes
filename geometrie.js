function computeSurface(radius){
    if(radius == null || undefined){
        throw new Error('missing parameter')
    }
    if(typeof radius === 'string'){
    
        radius = parseInt(radius)
    }
    if(Number.isNaN(radius)){
        throw new Error('invalid parameter')
    }

    return 3.14 * radius
}

export { computeSurface }