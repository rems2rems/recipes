
const recipes = []

const Recipe = function (recipe) { this.recipe = recipe }
Recipe.prototype.save = function() {
    recipes.push(this.recipe)
    return Promise.resolve("OK")
}
Recipe.find = function() {
    return Promise.resolve([...recipes])
}

export { Recipe }