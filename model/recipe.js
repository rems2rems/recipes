import mongoose from 'mongoose'
const difficultyEnum = ['Easy','Medium','Hard']
const priceEnum = [1,2,3,4,5]
const recipeSchema = mongoose.Schema({
    name :String,
    difficulty : {type:String,enum:difficultyEnum},
    withOven : Boolean,
    price : {type:Number,enum:priceEnum}
})

const Recipe = mongoose.model("Recipe",recipeSchema)

export { Recipe,recipeSchema,difficultyEnum,priceEnum}