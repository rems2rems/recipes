import { Builder, Capabilities } from 'selenium-webdriver'
import { Recipe } from '../../model/recipe.js'

function createDriver() {
    return new Builder()
      .usingServer(process.env.BROWSER_URL)
      .withCapabilities(Capabilities.firefox())
      .build()
  }
  
  async function resetRecipe() {
    let recipe = await Recipe.findOne({ name: 'Brocolis' })
    if (!recipe) {
      recipe = new Recipe({ name: 'Brocolis' })
      await recipe.save()
    }
    recipe.difficulty = 'Easy'
    await recipe.save()
  }

  export {createDriver,resetRecipe}
