import { By } from 'selenium-webdriver'
import { expect } from 'chai'
import mongoose from 'mongoose'
import { createDriver, resetRecipe } from './testsCommon.js'

describe('the recipes app', function () {
  this.timeout(8000)
  
  it('must list recipes', async function () {
    console.log("test list recipes...");
	const driver = createDriver()
    console.log("driver ok...");
	await mongoose.connect(process.env.DB + "?connectTimeoutMS=5000")
	console.log("db ok...");
	await resetRecipe()
    await driver.get(process.env.SERVER_URL)
    console.log("webapp ok...");
	const firstResult = await driver.findElement(By.css(".row:nth-child(2) .name"))
    expect(firstResult).to.exist
    console.log("exiting list recipes test...");
    return driver.quit();
  })
})
