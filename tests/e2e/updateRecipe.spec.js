import dotenv from 'dotenv'
dotenv.config({ path: '../../.env' })
console.log("DB=", process.env.DB);
console.log("SERVER=", process.env.SERVER_URL);
console.log("BROWSER=", process.env.BROWSER_URL);

import { By } from 'selenium-webdriver'
import { expect } from 'chai'
import mongoose from 'mongoose'
import { createDriver,resetRecipe } from './testsCommon.js'


describe('the recipes app', function () {
  this.timeout(8000)
  
  it('must update a recipe difficulty', async function () {
    await mongoose.connect(process.env.DB + "?connectTimeoutMS=5000")
    await resetRecipe()
    const driver = createDriver()
    const initialDifficulty = "Easy"
    const finalDifficulty = "Medium"

    await driver.get(process.env.SERVER_URL)
    let brocolis = await driver.findElement(By.css(".row:nth-child(2) .difficulty"))
    expect(brocolis).to.exist
    expect(await brocolis.getAttribute('textContent')).to.equal(initialDifficulty)
    let editBtn = await driver.findElement(By.css(".row:nth-child(2) > .btn"))
    await editBtn.click()

    const dropdown = await driver.findElement(By.name("difficulty"))
    await dropdown.findElement(By.xpath("//option[. = '" + finalDifficulty + "']")).click()
    // await driver.findElement(By.css("option:nth-child(1)")).click()
    const submitBtn = await driver.findElement(By.css(".btn"))
    await submitBtn.click()

    brocolis = await driver.findElement(By.css(".row:nth-child(2) .difficulty"))
    expect(brocolis).to.exist
    expect(await brocolis.getAttribute('textContent')).to.equal(finalDifficulty)
    await driver.quit();
    await mongoose.disconnect()
    console.log("exiting update recipe test...");
  })
})

