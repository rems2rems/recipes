import { difficultyEnum, priceEnum } from '../../model/recipe.js'
import { Recipe } from '../../model/mockRecipe.js'
import { createControllers } from '../../controllers/recipes.js'
const recipesControllers = createControllers(Recipe, difficultyEnum, priceEnum)
import { createRouter } from '../../routes/recipes.js'
const recipesRouter = createRouter(recipesControllers)

import chai from 'chai'
const expect = chai.expect

import request from 'supertest'
import express from 'express'

describe('the recipes router', () => {
    it('must return recipes on / route', async () => {

        const app = express();
        app.use('/recipes', recipesRouter)

        const recipe = new Recipe({ name: 'brocolis' })
        await recipe.save()
        const response = await request(app)
            .get('/recipes')
            .set('Accept', 'application/json')
        
        expect(response.headers["content-type"]).to.match(/json/);
        expect(response.status).to.equal(200);
        expect(response.body[0].name).to.equal('brocolis');
    });
});

