import autocannon from 'autocannon'
import chai from 'chai'
const expect = chai.expect

describe('the app perfs', function() {
    this.timeout(60000)
    it('must conform requirements', function(done) {
        autocannon({
            url: process.env.SERVER_URL,
            connections: 10, //default
            pipelining: 1, // default
            duration: 10 // default
        }, (err,results) => {
            expect(err).to.not.be.ok
            expect(results.requests.mean).to.be.above(3000)
            expect(results.latency.mean).to.be.below(2)
            expect(results.throughput.mean).to.be.above(100000)
            done()
        })
    });
});

