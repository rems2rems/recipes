import { expect } from 'chai'
import { categorizeRecipe } from '../../services/categorizeRecipe.js'

describe("a category", () => {
    it("must be tagged as difficult", () => {
        let recipe = {
            _id: "12344556",
            name: "couscous",
            ingredients: ["semoule", "pois chiches", "merguez"],
            needsOven: true,
            needsSpecializedTool: true,
            needsExoticFood: true,
            instructions:["faites du couscous, c'est bon"]
        }
        let category = categorizeRecipe(recipe)
        expect(category).to.equal("difficult")
    })
    it("mon 2eme test", () => {
        expect(1).to.equal(1)
    })
})