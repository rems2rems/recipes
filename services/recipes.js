import { randomUUID } from 'node:crypto'

let recipes = []

function listRecipes() {
    return recipes
}

function createRecipe(recipe) {
    if (!recipe._id) {
        recipe._id = randomUUID()
    }
    recipes.push(recipe)
}

export { createRecipe,listRecipes }