
function createControllers(Recipe, difficultyEnum, priceEnum) {

    const updateFn = async (req, res) => {
        await Recipe.findByIdAndUpdate(req.params.id, req.body)
        res.redirect("/")
    }

    const deleteFn = (req, res) => {
        Recipe.findByIdAndRemove(req.params.id)
        res.redirect("/")
    }

    const editFn = async (req, res) => {
        const recipe = await Recipe.findOne({ _id: req.params.id })
        res.render("editRecipe", { recipe, difficultyEnum, priceEnum })
    }

    const showFn = (req, res) => {
        const recipe = recipes.filter((r) => r.id == req.params.id)[0]
        res.render("showRecipe", { recipe })
    }

    const listFn = async (req, res) => {

        const recipes = await Recipe.find({})
        const acceptedFormats = req.get('Accept').split(",")
        if(acceptedFormats.includes('text/html')){
            res.render("listRecipes", { recipes, difficultyEnum, priceEnum })
        }
        else{
            res.set('Content-Type','application/json')
            res.json(recipes)
        }
    }

    /**
     * 
     * @param {*} req 
     */
    const createFn = (req) => {
        console.log("body?",!!req.body);
        if (req.body) {
            console.log(req.body.name);
            console.log(req.body.difficulty);
        }
    }

    return { createFn, listFn, showFn, deleteFn, editFn, updateFn }
}

export { createControllers }